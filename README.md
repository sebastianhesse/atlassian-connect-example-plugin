# Atlassian Connect Example Plugin

**Author:** Sebastian Hesse

This plugin is a simple example for JIRA and Confluence Cloud add-ons. It was created using [Atlassian Connect Express](https://bitbucket.org/atlassian/atlassian-connect-express/).
You can use and modify it if you want.