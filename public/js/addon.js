/* add-on script */
$(function() {
    AP.getUser(function(user) {
        $('#username').html(user.fullName);

        AP.require(['request'], function(request) {
            request({
                url: '/rest/api/latest/user?key=' + user.key,
                success: function(response) {
                    // show it to the user
                    $("#jsonUser").html(response);

                    // use the response for further operations
                    response = JSON.parse(response);
                }
            })
        });
    });
});